################ Makefile #################

SRC=main.c
CC=gcc
CFLAGS=-W -Wall
EXEC=adaline
OBJ=matrix.o main.o


all: $(EXEC)
$(EXEC): $(OBJ)
	$(CC) -o $@ $^

%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)

main.o: $(SRC) matrix.h
	$(CC) -o $@ -c $< $(CFLAGS)
.PHONY:
	clean mrproper
clean:
	rm -rf *.o
mkproper: clean
	rm -rf $(EXEC)
