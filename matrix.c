#include "matrix.h"
#include <stdio.h>
#include <stdlib.h>

/*
 * Get file input and load it as int matrix
 * This function expects a comma-separated (csv) file
 * DO NOT add the bias value to input
 * The last colum is considered the target out value
 * */
dyn_float_matrix readCsvAsMatrix(char* filePath)
{
	char character;
	dyn_float_matrix result;
	int buf = 0, column = 0;
	char* buffer = (char*) malloc(0 * sizeof(char));
	float* line = (float*) malloc(0 * sizeof(float));
	result.base = (float**) malloc(0 * sizeof(float*));
	result.rows = 0; result.cols = 0;

	FILE *fp = fopen(filePath, "r");

	if(fp == NULL) {
		printf("Could not open file in path '%s'\n", filePath);
		exit(-1);
	}

	while ((character = getc(fp)) != EOF) {
		if(character != ';' && character != '\n') {
			buffer = (char *) realloc(buffer, (buf+1) * sizeof(char));
			buffer[buf] = character;
			buf++;
		}
		else if(character == ';') {
			line = (float*) realloc(line, (column+1) * sizeof(float));
			line[column] = atof(buffer);
			column++;
			buf = 0;
			free(buffer);
			buffer = (char*) malloc(0 * sizeof(char));
		}
		else if(character == '\n') {
			line[column] = atof(buffer);
			buf = 0;
			free(buffer);
			buffer = (char*) malloc(0 * sizeof(char));

			result.base = (float**) realloc(result.base, (result.rows+1) * sizeof(float*));
			result.base[result.rows] = line;
			result.rows++;
			result.cols = column + 1;
			column = 0;
			line = (float*) malloc(0 * sizeof(float));

		}
	}
	fclose(fp);
	return result;
}
