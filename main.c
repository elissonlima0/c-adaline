#include <stdio.h>
#include <stdlib.h>
#include "matrix.h"

int main()
{
	dyn_float_matrix input = readCsvAsMatrix("input_train.txt");

	for (int i = 0; i < input.rows ; i++) {
		for (int j = 0; j < input.cols; j++)
			printf("%.2f ", input.base[i][j]);
		printf("\n");
	}

	return 0;

}
