#ifndef MATRIX_H__
#define MATRIX_H__

typedef struct dyn_float_matrix {
	float** base;
	int rows;
	int cols;
} dyn_float_matrix;

dyn_float_matrix readCsvAsMatrix(char* filePath);

#endif
